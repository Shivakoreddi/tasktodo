employees = [
    {"id": 1, "name": "John", "department_id": 1},
    {"id": 2, "name": "Alice", "department_id": 2},
    {"id": 3, "name": "Bob", "department_id": 1},
    {"id": 4, "name": "Emma", "department_id": 3},
    {"id": 5, "name": "Michael", "department_id": 2}
]

departments = [
    {"id": 1, "name": "Sales"},
    {"id": 2, "name": "Marketing"},
    {"id": 3, "name": "Finance"}
]


#Can you create a new dataset combining employee details with their respective department names?

def deptEmp(dept,emp):
    result = []
    record={}
    for d in dept:
        dname = d['name']
        id = d['id']
        for e in emp:
            ename = e['name']
            eid = e['id']
            did = e['department_id']
            if id == did:
                if ename not in record:
                    record[ename] = (did,dname)
    for ename,values in record.items():
        result.append({'emp':ename,'dept_id':values[0],'dept_name':values[1]})
    return result

print(deptEmp(departments,employees))
                
    
    
    
    
    
    
    
    
    
    
