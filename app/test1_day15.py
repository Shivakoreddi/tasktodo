social_media_posts = [
    {"id": 1, "username": "user1", "post": "Excited to announce our new product launch! #newproduct #excitingtimes"},
    {"id": 2, "username": "user2", "post": "Had a great time at the concert last night! 🎵🎉 #music #fun"},
    {"id": 3, "username": "user3", "post": "Just finished reading an amazing book. Highly recommended! 📚 #reading #recommendation"},
    {"id": 4, "username": "user4", "post": "Visited a beautiful place today. Nature at its best! 🌿🏞️ #nature #travel"},
    {"id": 5, "username": "user5", "post": "Trying out a new recipe. Finger-licking good! 🍽️ #cooking #foodie"}
]


#determine average length of post

#result = [{'avg_length':20}]


def avgLength(social_media):
    result = {}
    post=0
    count=0
    for i in social_media:
        post+=len(i['post'])
        count=count+1 
    result['avg_length']=float(post/count)
    return result

print(avgLength(social_media_posts))
