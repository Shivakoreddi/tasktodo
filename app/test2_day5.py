sales = [('1jan','tables',200),
         ('2jan','chairs',200),
         ('3jan','stool',40),
         ('4jan','lamp',40),
         ('5jan','fan',300),
         ('6jan','tables',200),
         ('7jan','fan',300),
         ('8jan','stool',40),
         ('9jan','lamp',40)]


#result = [('tables',400),('chairs',200),('stool',80),('lamp',80),('fan',600)]

def salesSum(sales):
    result = []
    record={}
    for i in range(len(sales)):
        date,item,sale=sales[i]
        if item not in record:
            record[item]=(item,sale)
        else:
            t= record[item]
            l = list(t)
            l[1]+=sale
            t=tuple(l)
            record[item]=t
    result = [(value) for value in record.values()]
    return result

print(salesSum(sales))

