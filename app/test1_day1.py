data =  [("1Jan", 100), 
        ("2Jan", 100), 
        ("3Jan", 200), 
        ("4Jan", 200), 
        ("5Jan", 300)]

expected_format = [("str_date","end_date","price")]


def group_data(data):
    output = []
    st_date,prev_price=data[0]
    for i in range(1,len(data)):
        date,price = data[i]
        if prev_price!=price:
            output.append((st_date,data[i-1][0],prev_price))
            st_date=date
        prev_price = price
    output.append((st_date,data[-1][0],prev_price))
    return output

print(group_data(data))
 
      
        
        
        
    
    