fin_transactions = [
    {"transaction_id": "T123456", "date": "2023-05-25", "amount": 1000.0, "type": "debit"},
    {"transaction_id": "T123457", "date": "2023-05-25", "amount": 500.0, "type": "credit"},
    {"transaction_id": "T123458", "date": "2023-05-26", "amount": 750.0, "type": "debit"},
    {"transaction_id": "T123459", "date": "2023-05-26", "amount": 2000.0, "type": "credit"},
    {"transaction_id": "T123460", "date": "2023-05-27", "amount": 1500.0, "type": "debit"}
]

client = [('sk',['T123456','T123459']),('ak',['T123457','T123458','T123460'])]

#generate output as name,balance - balance is calculated by debits(+) and credits(-)
#result = [{'name':'sk','balance':-1000},{'name':'ak','balance':1750}]


#alg
#1. read fin_transactions - id, type,amount 
#2. read client - name,transaction_id 
#3. join both datasets with trans_id into single where it can contain - name,transaction_id,type,amount 
#4. now perform operation on trans_dataset


def finProcess(fin_transactions,client):
    result = []
    for tran in fin_transactions:
        id = tran['transaction_id']
        amount = tran['amount']
        type = tran['type']
        if type=="debit":
            amt = amount
        else:
            amt = -(amount)
        for i in range(len(client)):
            
            name,id_list=client[i]
            if id in id_list:
                result.append((name,id,amt))
    return result

def calBalance(dataset):
    bal = []
    record = {}
    for i in range(len(dataset)):
        name,id,amt = dataset[i]
        if name not in record:
            record[name]=amt
        else:
            record[name]+=amt
    for name,amt in record.items():
        bal.append({'name':name,'balance':amt})
         
    return bal

dataset = finProcess(fin_transactions,client)
print(calBalance(dataset))            
            
            
            
            
            
        
    