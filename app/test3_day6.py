sampleDict = {
    "class": {
        "student": {
            "name": "Mike",
            "marks": {
                "physics": 70,
                "history": 80
            }
        }
    }
}


#parse key history from dictionary


def parseDict(sampleDict):
    class1 = sampleDict['class']
    student = class1['student']
    marks=student['marks']
    history=marks['history']
    
    return history

print(parseDict(sampleDict))

