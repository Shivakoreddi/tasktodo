person = [(1,"a@b.com"),(2,"c@d.com"),(3,"a@b.com")]

#find duplicate email from person table
#expected result = [("a@b.com")]

#alg
def removeDups(person):
    items = {}
    result = []
    for id,email in person:
        if email in items:
            items[email]+=1
        else:
            items[email]=1
    for key,value in items.items():
        if items[key]>1:
            result.append((key))  
    return result

print(removeDups(person))

