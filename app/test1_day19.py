orders = [
    {"order_id": 1, "product_id": 1, "quantity": 2},
    {"order_id": 2, "product_id": 2, "quantity": 1},
    {"order_id": 3, "product_id": 3, "quantity": 3},
    {"order_id": 4, "product_id": 1, "quantity": 1},
    {"order_id": 5, "product_id": 4, "quantity": 2}
]

products = [
    {"id": 1, "name": "Laptop", "price": 1000},
    {"id": 2, "name": "Smartphone", "price": 800},
    {"id": 3, "name": "Headphones", "price": 150},
    {"id": 4, "name": "TV", "price": 1500},
    {"id": 5, "name": "Tablet", "price": 1200}
]


#Can you create a new dataset combining order details with the respective product details?
#result=[{id,name,price,order_qty,total_cost}]

def proOrder(order,product):
    result = []
    record={}
    for o in order:
        pid = o['product_id']
        qty = o['quantity']
        
        for p in product:
            id = p['id']
            name = p['name']
            price = p['price']
            if pid == id:
                if pid not in record:
                    
                    record[pid]=qty*price
                else:
                    record[pid]+=qty*price 
    result.append(record)
    
    return result


print(proOrder(orders,products))
            
        
        
        
        