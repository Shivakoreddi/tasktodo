sales = [(1,'1jan','tables',1,200),
         (2,'4jan','chairs',4,200),
         (3,'5jan','stool',2,40),
         (4,'5jan','lamp',4,40),
         (5,'8jan','fan',3,300)]


#result = [('1jan',200),('4jan',200),('5jan',80),('8jan',300)]

def salesSum(sales):
    result = []
    record={}
    for i in range(len(sales)):
        id,date,item,qty,sold=sales[i]
        if date not in record:
            record[date]=(date,sold)
        else:
            t=record[date]
            l = list(record[date])
            l[1]+=sold
            t= tuple(l)
            record[date]=t
        
    result = [(value) for key,value in record.items()]
    return result


print(salesSum(sales))
            