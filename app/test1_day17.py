employees = [
    {"id": 1, "name": "John", "department_id": 1},
    {"id": 2, "name": "Alice", "department_id": 2},
    {"id": 3, "name": "Bob", "department_id": 1},
    {"id": 4, "name": "Emma", "department_id": 3},
    {"id": 5, "name": "Michael", "department_id": 2}
]

departments = [
    {"id": 1, "name": "Sales"},
    {"id": 2, "name": "Marketing"},
    {"id": 3, "name": "Finance"}
]


#How many employees are there in each department?

def deptCount(emp,dept):
    result = []
    record={}
    for d in dept:
        id = d['id']
        dname=d['name']
        dcount=1
        for e in emp:
            did = e['department_id']
            if d['id']==e['department_id']:
                if dname not in record:
                    record[dname]=dcount
                else:
                    record[dname]+=dcount
    result.append(record)
    return result

print(deptCount(employees,departments))