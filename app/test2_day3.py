customers = [{'id':1,'name':'Joe'},
             {'id':2,'name':'Henry'},
             {'id':3,'name':'Sam'},
             {'id':4,'name':'Max'},
             {'id':5,'name':'John'}]

orders = [{'id':1,'customerId':3},
          {'id':2,'customerId':1},
          {'id':3,'customerId':2}]

#write function find customers who never order anything

#expected result = [{'Henry','Max'}]

def noOrder(customers,orders):
    result = []
    cust = set()
    cust1=set()
    for d1 in customers:
        for d2 in orders:
            if d1['id']==d2['customerId']:
                cust.add(d1['name'])
            else:
                cust1.add(d1['name'])
    c = cust1-cust        
    result.append(c)
    return result

print(noOrder(customers,orders))
                