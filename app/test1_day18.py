customers = [
    {"id": 1, "name": "John", "country_id": 1},
    {"id": 2, "name": "Alice", "country_id": 2},
    {"id": 3, "name": "Bob", "country_id": 2},
    {"id": 4, "name": "Emma", "country_id": 3},
    {"id": 5, "name": "Michael", "country_id": 1}
]

countries = [
    {"id": 1, "name": "USA"},
    {"id": 2, "name": "UK"},
    {"id": 3, "name": "Canada"}
]


#Can you create a new dataset combining customer details with their respective country names?


# k[//l]zo0i    @QSQ1``` `                         
# ]93656RTR66Gbw x9,,,,,,  w  sJT`qz `
# yafDc87uh26ysc vm 

def customerCon(customer,country):
    result = []
    record={}
    for c in country:
        id = c['id']
        name = c['name']
        for e in customer:
            cid=e['country_id']
            if id==e['country_id']:
                if name not in record:
                    record[name]=1
                else:
                    record[name]+=1
    result.append(record)
    return result

print(customerCon(customers,countries))            


