raw_ticket = [("QW0E00_54204554_01"),
              ("QW0E11_54204564_01"),
              ("QW0E00_54204555_02"),
              ("QW0E11_54204534_01"),
              ("QW0F01_54204524_01"),
              ("QW0F01_54204514_01")]

raw_str = """'QW0E00_54204554_01',
    'QW0E11_54204564_01',
    'QW0E00_54204555_02',
    'QW0E11_54204534_01',
    'QW0F01_54204524_01',
    'QW0F01_54204514_01'"""

#read above dataset in different variables 


def rawRecord(raw_ticket):
    result = []
    record = {}
    for i in raw_ticket:
        pnr,tran,ts = i.split("_")
        record[tran] = (pnr,tran,ts)
    for tran,values in record.items():
        result.append({'pnr':values[0],'transaction':tran,'timestamp':values[2]})
    return result

print(rawRecord(raw_ticket))
    
