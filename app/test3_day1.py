
##log = [(id,value)]


##expected = [consecutive_value] 
# output= [1]

##find atleast 3 times consecutive value 


def readCon(log):
    consec = []
    prev_val = log[0][1]
    count = 1
    for id,val in log:
        if val==prev_val:
            count+=1
        else:
            if count>=3:
                consec.append(prev_val)
            count=1
        prev_val=val
        
    if count>=3:
        consec.append(prev_val)   
        return consec

log = [(1,1),(2,1),(3,1),(4,3),(5,2),(6,2),(7,2)]

print(readCon(log))

