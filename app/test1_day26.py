import pandas as pd
import pandas.testing as pd_testing
import csv
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Paragraph, Spacer
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import Table, TableStyle
from reportlab.lib.enums import TA_CENTER, TA_LEFT


def generate_pdf_report(validation_frame, output_file):
    # Create a new PDF document
    doc = SimpleDocTemplate(output_file, pagesize=letter)

    # Add the content to the PDF document
    story = []

    # Add the title to the report (using a custom style)
    title_style = getSampleStyleSheet()['Title']
    

    for key,frame in validation_frame.items():
        story.append(Spacer(1,12))
        if key=='column':
            title = Paragraph("Mismatched Columns and Values Report", title_style)
            story.append(title)
            data_list = [frame.columns.astype(str).tolist()] + frame.astype(str).values.tolist()
        elif key=='full':
            title = Paragraph("Mismatched Records and Values Report", title_style)
            story.append(title)
            data_list = [frame.columns.astype(str).tolist()] + frame.astype(str).values.tolist()

        # Create a table with the mismatched columns and values
        table = Table(data_list, hAlign='LEFT')

        # Add more styling options to the table
        table_style = TableStyle([
            ('BACKGROUND', (0, 0), (-1, 0), colors.grey),
            ('TEXTCOLOR', (0, 0), (-1, 0), colors.whitesmoke),
            ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
            ('FONTNAME', (0, 0), (-1, 0), 'Helvetica-Bold'),  # Bold font for header
            ('FONTSIZE', (0, 0), (-1, 0), 12),
            ('BOTTOMPADDING', (0, 0), (-1, 0), 12),
            ('BACKGROUND', (0, 1), (-1, -1), colors.beige),
            ('GRID', (0, 0), (-1, -1), 1, colors.black),
            ('ALIGN', (0, 0), (-1, 0), 'CENTER'),  # Center align header
            ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),  # Middle align cell content
            ('TEXTCOLOR', (0, 1), (-1, -1), colors.black),  # Black text for cell content
            ('FONTNAME', (0, 1), (-1, -1), 'Helvetica'),  # Regular font for cell content
            ('BACKGROUND', (0, 1), (-1, 1), colors.lightblue),  # Light blue background for first row of data
            ('BACKGROUND', (0, 2), (-1, -1), colors.white),  # White background for rest of the data
        ])

    table.setStyle(table_style)

    # Add a space between the title and the table
    story.append(Spacer(1, 12))

    # Add the table to the story
    story.append(table)

    # Build the PDF document
    doc.build(story)

    print(f"PDF report generated and saved to '{output_file}'.")

# Rest of the code remains the same...



def compare_columns(df1,df2):
    column_data=[]
    for column in df1.columns:
        if not (df1[column]==df2[column]).all():
            mis_df1 = df1.loc[df1[column]!=df2[column],column].values
            mis_df2 = df2.loc[df2[column]!=df1[column],column].values
            mis_id = [i for i,(val1,val2) in enumerate(zip(df1[column],df2[column])) if val1!=val2]
            column_data.extend(zip([column]*len(df1),mis_df1.tolist(),mis_df2.tolist(),mis_id))
        if column_data:
            mis_column = pd.DateFrame(column_data,column=['Column','df1_value','df2_value','rowid'])
        
            return mis_column
    return pd.DataFrame()
    
def compare_csv_files(file1, file2, output_file):
    # Read both CSV files into DataFrames
    df1 = pd.read_csv(file1)
    df2 = pd.read_csv(file2)

    # Validate if both DataFrames have the same columns
    if not df1.columns.equals(df2.columns):
        print("Columns in the two CSV files do not match.")
        print("Columns in the first CSV file:", df1.columns)
        print("Columns in the second CSV file:", df2.columns)
        return

    # Concatenate the DataFrames along rows
    concatenated_df = pd.concat([df1, df2], ignore_index=True)

    # Identify duplicate rows (matching records) based on all columns
    duplicate_rows = concatenated_df[concatenated_df.duplicated(keep=False)]

    # Filter mismatched records (rows that are not duplicate, i.e., unique to either DataFrame)
    mismatched_records = concatenated_df.drop_duplicates(keep=False)
    column_df = compare_columns(df1,df2)
    validation_frames = {'column':column_df,'full':mismatched_records}
    if not mismatched_records.empty:
        print("Mismatched records between the two CSV files:")
        print(mismatched_records)

        # Write mismatched records to CSV file
        # mismatched_records.to_csv(output_file, index=False)
        generate_pdf_report(validation_frames,output_file)
        print(f"Mismatched records have been written to '{output_file}'.")
    else:
        print("No mismatched records found between the two CSV files.")

if __name__ == "__main__":
    file1 = 'file1.csv'
    file2 = 'file2.csv'
    output_file = 'data_validation.pdf'

    compare_csv_files(file1, file2, output_file)
