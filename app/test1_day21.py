products = [
    {"id": 1, "name": "Laptop", "brand_id": 1},
    {"id": 2, "name": "Smartphone", "brand_id": 2},
    {"id": 3, "name": "Headphones", "brand_id": 3},
    {"id": 4, "name": "TV", "brand_id": 4},
    {"id": 5, "name": "Tablet", "brand_id": 5}
]

brands = [
    {"id": 1, "name": "Dell"},
    {"id": 2, "name": "Samsung"},
    {"id": 3, "name": "Sony"},
    {"id": 4, "name": "LG"},
    {"id": 5, "name": "Apple"}
]



#Can you create a new dataset combining product details with their respective brand names?


def proBrand(products,brands):
    result = []
    record={}
    for b in brands:
        bid = b['id']
        bname = b['name']
        for p in products:
            pname = p['name']
            id = p['brand_id']
            if id == bid:
                if pname not in record:
                    record[pname]=(id,bname)
    for pname,values in record.items():
        result.append({'product':pname,'brand_id':values[0],'brand':values[1]})
    return result

print(proBrand(products,brands))