sales = [(1,'1jan','tables',1,200),
         (2,'4jan','chairs',4,200),
         (3,'5jan','stool',2,40),
         (4,'5jan','lamp',4,40),
         (5,'8jan','fan',3,300)]

#result= [{'date':'1jan','items':['tables'],'sold':200},
#           {'date':'4jan','items':['chairs'],'sold':200},
#           {'date':'5jan','items':['stool','lamp'],'sold':80},
#           {'date':'8jan','items':['fan'],'sold':300}]


def salesAgg(sales):
    result=[]
    record={}
    for i in range(len(sales)):
        id,date,item,qty,sale=sales[i]
        
        
        if date not in record:
            record[date]={'items':[item],'sold':sale}
            
        else:
            record[date]['items'].append(item)
            record[date]['sold']+=sale
            
    result = [{'date':date,**value} for date,value in record.items()]     
        
        
    return result

print(salesAgg(sales))