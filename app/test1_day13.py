financial = [{'timestamp':'2023-03-12','fintype':'insurance','custid':'1A','activity':'buy','qty':2,'cost':2000},
             {'timestamp':'2023-03-13','fintype':'insurance','custid':'1A','activity':'sell','qty':1,'cost':800},
             {'timestamp':'2023-03-14','fintype':'bonds','custid':'2A','activity':'buy','qty':2,'cost':2000},
             {'timestamp':'2023-03-15','fintype':'bonds','custid':'2A','activity':'sell','qty':1,'cost':1000},
             {'timestamp':'2023-03-16','fintype':'bonds','custid':'2B','activity':'buy','qty':5,'cost':5000},
             {'timestamp':'2023-03-17','fintype':'insurance','custid':'2B','activity':'buy','qty':2,'cost':2000}]


#calculate account balance by qty,amount per customer id

#result = [{'custid':'1A','qty':1,'amount':1200},
#           {'custid':'2A','qty':1,'amount':1000}]


def financeBalance(finance):
    result = []
    record={}
    for i in finance:
        custid = i['custid']
        act = i['activity']
        if act =='buy':
            a = i['cost']
            q = i['qty']
        else:
            a = -(i['cost'])
            q = -(i['qty'])
        if custid not in record:
            record[custid]=(q,a)
        else:
            qa = list(record[custid])
            qa[0]+=q
            qa[1]+=a
            record[custid]=tuple(qa)
    for custid,values in record.items():
        result.append({'custid':custid,'qty':values[0],'amount':values[1]})
    return result


print(financeBalance(financial))
            