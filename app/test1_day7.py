students = [{'name':'sk','class':101,'course':'maths'},
            {'name':'ak','class':101,'course':'science'},
            {'name':'cr','class':102,'course':'english'},
            {'name':'sk','class':102,'course':'english'},
            {'name':'ak','class':102,'course':'hindi'}]

#result = [{'name':'sk','course':['maths','english']},
#           {'name':'ak','course':['science','hindi']},
#           {'name':'cr','course':['english']}]


def courseList(students):
    result = []
    record = {}
    for student in students:
        name = student['name']
        course = student['course']
        if name not in record:
            record[name]=[course]
            
        else:
            record[name].append(course)
    for name,course in record.items():
        result.append({'name':name,'course':course})
    return result

print(courseList(students))
        