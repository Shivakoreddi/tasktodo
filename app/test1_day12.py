log_files = [
    {"timestamp": "2023-05-25 10:05:32", "message": "INFO: Application started successfully"},
    {"timestamp": "2023-05-25 10:08:45", "message": "ERROR: An unexpected error occurred"},
    {"timestamp": "2023-05-25 10:12:15", "message": "WARNING: Disk space is running low"},
    {"timestamp": "2023-05-25 10:16:09", "message": "INFO: User logged in"},
    {"timestamp": "2023-05-25 10:21:03", "message": "DEBUG: Processing request"}
]

#Filter logs based on specific criteria (e.g., errors only).

#result = [{'timestamp':"2023-05-25 10:05:32",'Message_type':'Info',Message_Details:Application started success},
# {'timestamp':"2023-05-25 10:05:32",'Message_type':'Error',Message_Details:An unexpected error occured},
# {'timestamp':"2023-05-25 10:05:32",'Message_type':'Debug',Message_Details:Processing Request}]


#read each record and break essage column into two columns message type, message details- perform string manipulation on message column

def messageDetails(log):
    result=[]
    for i in log:
        message_type,message_details=i['message'].split(': ')
        result.append({'timestamp':i['timestamp'],'message_type':message_type,'message_details':message_details})
    return result

print(messageDetails(log_files))






