employee = [{'id':1,'name':'Joe','salary':'70000','departmentId':1},
            {'id':2,'name':'Jim','salary':'90000','departmentId':1},
            {'id':3,'name':'Henry','salary':'80000','departmentId':2},
            {'id':4,'name':'Sam','salary':'60000','departmentId':2},
            {'id':5,'name':'Max','salary':'90000','departmentId':1}]

department = [{'id':1,'name':'IT'},
              {'id':2,'name':'Sales'}]


#write function to find employees who have the highest salary in each department

# result = [{'department':'IT','Employee':'Jim','Salary':90000},
#            {'department':'Sales','Employee':'Henry','Salary':80000},
#            {'department':'IT','Employee':'Max','Salary':90000}]


def highSalary(employee,department):
    
    # for d1 in employee:
    #     for d2 in department:
    #         if d1['departmentId']==d2['id']:
    #             record = {'department':d2['name'],'Employee':d1['name'],'Salary':d1['salary']}
    #             result.append(record)'
    max_salary = {}
    for emp in employee:
        dept_id = emp['departmentId']
        name = emp['name']
        salary = emp['salary']
        if dept_id not in max_salary or salary>max_salary[dept_id][1]:
            max_salary[dept_id]=(name,salary)
    return max_salary

print(highSalary(employee,department))



            
    
    


                
                
        
        
        