
score =[{'branch': 'COE', 'cgpa': '9.0',
          'name': 'Nikhil', 'year': '2'},
        {'branch': 'COE', 'cgpa': '9.1',
         'name': 'Sanchit', 'year': '2'},
        {'branch': 'IT', 'cgpa': '9.3',
         'name': 'Aditya', 'year': '2'},
        {'branch': 'SE', 'cgpa': '9.5',
         'name': 'Sagar', 'year': '1'},
        {'branch': 'MCE', 'cgpa': '7.8',
         'name': 'Prateek', 'year': '3'},
        {'branch': 'EP', 'cgpa': '9.1',
         'name': 'Sahil', 'year': '2'}]


#result = [{'branch':'COE','avg_cgpa':9.05},{'branch':'IT','avg_cgpa':9.3}]


def scoreAgg(score):
  result = []
  record={}
  count={}
  for i in score:
    branch = i['branch']
    cgpa=float(i['cgpa'])
    
    if  branch in record:
      record[branch]+=cgpa
      count[branch]+=1
    else:
      record[branch]=cgpa
      count[branch]=1
  for branch in record:
    avg_cgpa = record[branch]/count[branch]
    result.append({'branch':branch,'avg_cgpa':avg_cgpa})
      
  return result

print(scoreAgg(score))
      
      
      
    
  


