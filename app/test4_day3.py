stocks = [('1jan',1000),('2jan',1000),('3jan',2000),('4jan',2000),('5jan',3000)]


#result = [('1jan','2jan',1000),('3jan','4jan',2000),('5jan','5jan',3000)]

#alg

# 1. store first record into initial variables
# 2. create result list
# 3. iterate from first row

def stockRange(stocks):
    result = []
    st_date,prev_price = stocks[0]
    
    for i in range(1,len(stocks)):
        date,price = stocks[i]
        if price!=prev_price:
            result.append((st_date,stocks[i-1][0],prev_price))
            st_date=date
        prev_price=price    
        
    result.append((st_date,stocks[-1][0],prev_price))
    return result
    
print(stockRange(stocks))