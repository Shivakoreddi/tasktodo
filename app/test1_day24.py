##compare two datasets

emp1= [
    {"id": 1, "name": "John", "department_id": 1},
    {"id": 2, "name": "Alice", "department_id": 2},
    {"id": 3, "name": "Bob", "department_id": 1},
    {"id": 4, "name": "Emma", "department_id": 3},
    {"id": 5, "name": "Michael", "department_id": 2}
]


emp2 = [
    {"id": 1, "name": "John", "department_id": 1},
    {"id": 2, "name": "Alice", "department_id": 2},
    {"id": 3, "name": "Bob", "department_id": 1},
    {"id": 4, "name": "Emma", "department_id": 3},
    {"id": 5, "name": "Michael", "department_id": 2}
]

##converts datasets to sets for easy comparision
##determine is datasets are equal
def compEmp(emp1,emp2):
    emp1_set = set((emp['id'],emp['name'],emp['department_id']) for emp in emp1)
    emp2_set = set((emp['id'],emp['name'],emp['department_id']) for emp in emp2)
    dataset_equal = emp1_set==emp2_set 
    return dataset_equal


print(compEmp(emp1,emp2))