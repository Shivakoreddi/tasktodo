sensor_data = [
    {"sensor_id": 1, "temperature": 25.3, "humidity": 60.2, "timestamp": "2023-05-25 08:15:20"},
    {"sensor_id": 2, "temperature": 28.1, "humidity": 55.9, "timestamp": "2023-05-25 08:20:15"},
    {"sensor_id": 1, "temperature": 26.7, "humidity": 61.5, "timestamp": "2023-05-25 08:25:10"},
    {"sensor_id": 3, "temperature": 24.8, "humidity": 57.3, "timestamp": "2023-05-25 08:30:05"},
    {"sensor_id": 2, "temperature": 27.9, "humidity": 58.6, "timestamp": "2023-05-25 08:35:00"}
]


#Identify sensors with the highest and lowest readings.


#result = [highest_temp:sensor_id,lowest_temp:sensor_id]


#alg
# 1. create record dict , max_temp varialbe 
# 2. iterate through sensor id and compare temp with previous max value if > assign it to max va else continue


def maxSensor(sensor):
    result = []
    record={}
    max_temp=0
    low_humid=float('inf')
    max_humid=0
    low_temp=float('inf')
    max_tid=0
    low_tid=0
    max_hid=0
    low_hid=0
    for i in sensor:
        id = i['sensor_id']
        temp = i['temperature']
        humid = i['humidity']
        
        # if id not in record:
        #     record[id]=(temp,temp,humid,humid)
        # else:
        if temp>max_temp:
            max_temp=temp
            max_tid=id
        if temp<low_temp:
            low_temp=temp
            low_tid=id
            
        if humid>max_humid:
            max_humid=humid
            max_hid=id
        if humid<low_humid:
            low_humid=humid
            low_hid=id
           
        
    record['max_temp']=max_temp
    record['max_tid']=max_tid
    record['max_humid']=max_humid
    record['max_hid']=max_hid
    record['low_temp']=low_temp
    record['low_tid']=low_tid
    record['low_humid']=low_humid
    record['low_hid']=low_hid
    
    return record 

print(maxSensor(sensor_data))
                
            
            
        
    