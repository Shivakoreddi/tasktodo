employee_data = [
    {"id": 1, "name": "John", "department": "Sales", "salary": 5000},
    {"id": 2, "name": "Alice", "department": "Marketing", "salary": 6000},
    {"id": 3, "name": "Bob", "department": "Finance", "salary": 5500},
    {"id": 4, "name": "Emma", "department": "Sales", "salary": 5200},
    {"id": 5, "name": "Michael", "department": "HR", "salary": 4800}
]


#Group employees by department and calculate the average salary for each department.

def avgSalary(employee):
    result = []
    record={}
    count={}
    for i in employee:
        dept=i['department']
        salary=i['salary']
        if dept not in record:
            record[dept]=salary
            count[dept]=1
        else:
            record[dept]+=salary
            count[dept]+=1
    for dept in record.keys():
        avg_salary=record[dept]=count[dept]
        result.append({'dept':dept,'avg_salary':avg_salary})
    return result

print(avgSalary(employee_data))
            
        

