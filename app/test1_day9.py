sensor_data = [
    {"sensor_id": 1, "temperature": 25.3, "humidity": 60.2, "timestamp": "2023-05-25 08:15:20"},
    {"sensor_id": 2, "temperature": 28.1, "humidity": 55.9, "timestamp": "2023-05-25 08:20:15"},
    {"sensor_id": 1, "temperature": 26.7, "humidity": 61.5, "timestamp": "2023-05-25 08:25:10"},
    {"sensor_id": 3, "temperature": 24.8, "humidity": 57.3, "timestamp": "2023-05-25 08:30:05"},
    {"sensor_id": 2, "temperature": 27.9, "humidity": 58.6, "timestamp": "2023-05-25 08:35:00"}
]


##Calculate the average temperature and humidity for each sensor.

##result = [{sensor,avg_temp,avg_humidity}]

def avgSensor(sensor):
    result = []
    record={}
    count = {}
    for i in sensor:
        id=i['sensor_id']
        temp = i['temperature']
        humid = i['humidity']
        if id not in record:
            record[id]=(temp,humid)
            count[id]=1
        else:
            count[id]+=1
            a_list = list(record[id])
            a_list[0]+=temp
            a_list[1]+=humid
            record[id]=tuple(a_list)
            
    for id in record.keys():
        avg_temp=record[id][0]/count[id]
        avg_humid=record[id][1]/count[id]
        result.append({'sensor':id,'avg_temp':avg_temp,'avg_humid':avg_humid})
    return result


print(avgSensor(sensor_data))
                
    



