financial = [{'timestamp':'2023-03-12','fintype':'insurance','custid':'1A','activity':'buy','qty':2,'cost':2000},
             {'timestamp':'2023-03-13','fintype':'insurance','custid':'1A','activity':'sell','qty':1,'cost':800},
             {'timestamp':'2023-03-14','fintype':'bonds','custid':'2A','activity':'buy','qty':2,'cost':2000},
             {'timestamp':'2023-03-15','fintype':'bonds','custid':'2A','activity':'sell','qty':1,'cost':1000},
             {'timestamp':'2023-03-16','fintype':'bonds','custid':'2B','activity':'buy','qty':5,'cost':5000},
             {'timestamp':'2023-03-17','fintype':'bonds','custid':'2B','activity':'sell','qty':2,'cost':2000}]



#calculate account balance by qty,amount per customer id

#result = [{'custid':'1A','type':'insurence','qty':1,'amount':1200},
#           {'custid':'2A','type':'insurence','qty':1,'amount':1000},
#           {'custid':'2B','type':'bonds','qty':3,'amount':3000}]


def financeType(finance):
    result=[]
    record={}
    for i in finance:
        custid = i['custid']
        act = i['activity']
        typ = i['fintype']
        ctype = "_".join([custid,typ])
        
        if act=='buy':
            q=i['qty']
            a = i['cost']
        else:
            q= -(i['qty'])
            a = -(i['cost'])
        if ctype not in record:
            
            record[ctype]=(a,q)
        else:
            alist = list(record[ctype])
            alist[0]+=a 
            alist[1]+=q 
            record[ctype]=tuple(alist)
    for ctype,value in record.items():
        
        cust,t = ctype.split("_")
        result.append({'custid':cust,'type':t,'qty':value[1],'amount':value[0]})
    return result


print(financeType(financial))