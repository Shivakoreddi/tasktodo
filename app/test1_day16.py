social = [(1,200,'sports',46,20,1500,800),
          (1,250,'movies',56,25,1800,1500),
          (1,150,'politics',86,18,600,1300),
          (1,80,'health',65,12,200,100),
          (1,120,'science',72,16,500,80)]


#[(rid,posts_count,post_ctegory,post_avg_length,max_commnt_count,max_likes,max_dislikes)]

#result=[{'category':'sports','likes':1500,'dislikes':800}]

def socialRead(social):
    result = []
    for i in range(len(social)):
        rid,posts_count,post_category,post_avg_length,max_commnt_count,max_likes,max_dislikes=social[i]
        result.append({'category':post_category,'likes':max_likes,'dislikes':max_dislikes})
    return result

print(socialRead(social))