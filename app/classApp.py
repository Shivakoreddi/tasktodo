#in this script we will define all the classes necessary and pull them in to our prgram when needed

class Tasktodo:
    def __init__(self):
        pass
    
    def searchNote(self):
        raise NotImplementedError("Subclass must be defined")
    
    def searchTask(self):
        raise NotImplementedError("Subclass must be defined")
    
    

class User(Tasktodo):
    
    def searchTask(self):
        pass
    
    def searchNote(self):
        pass
    
    def note(self,date,file):
        #file variable is file location where the note file will be stored
        #date variable is date when note created
        #user can create one note per day
        
        pass
    
    def task(self,note):
        #note to which the task belongs to
        #once task is created with details then it will be written back to note file
        pass
            
    
    
   ADF