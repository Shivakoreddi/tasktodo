projects = [
    {"Alice": {"Project A", "Project B"}},
    {"Bob": {"Project B", "Project C"}},
    {"Alice": {"Project C", "Project D"}},
    {"Charlie": {"Project A", "Project D"}},
    {"Bob": {"Project A", "Project B"}},
    {"Eve": {"Project B", "Project D"}}
]


#Find the projects that involve all employees.

def allProject(projects):
    result = []
    record={}
    for p in projects:
        name = list(p.keys())[0]
        pro = list(p.values())
        if name not in record:
            record[name]=[pro]
        else:
            record[name].append(pro)
    return record

print(allProject(projects))
